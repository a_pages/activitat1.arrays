<?php
/**
 * Created by PhpStorm.
 * User: abel
 * Date: 22/10/2016
 * Time: 10:41
 */

function crearMatriu($n) {
    /*doble for per emplenar l'array i mostrar la matriu al mateix temps*/
    for ($i = 0; $i < $n; $i++) {
        for ($j = 0; $j < $n; $j++) {
            if ($i > $j) {
                $quadrat[$j][$i] = $i +$j;
            } elseif ($i == $j) {
                $quadrat[$j][$i] = "*";
            } else {
                $quadrat[$j][$i] = rand(10,20);
            }
        }
    }
    return $quadrat;
}

function girarMatriu($matriu) {
    for ($i =  0;$i < sizeof($matriu); $i++) {
        for ($j =  0;$j < sizeof($matriu); $j++) {
            $girat[$i][$j]  = $matriu[$j][$i];
        }
    }
    return $girat;
}

function mostrarQuadrat($matriu) {
    echo "<table style='border: solid 1px; border-collapse: collapse;'>";
    foreach ($matriu as $fila) {
        echo "<tr style='border: solid 1px; border-collapse: collapse;'>";
        foreach ($fila as $columna) {
            echo "<td style='border: solid 1px; border-collapse: collapse;'>";
            echo $columna;
            echo "</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
}
$matriu = crearMatriu(4);
echo "<br>";
echo mostrarQuadrat($matriu);
echo "<br>";
echo mostrarQuadrat(girarMatriu($matriu));